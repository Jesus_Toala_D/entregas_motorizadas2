---Procedimiento
create or replace function totalcliente(varchar)
returns setof "record"
as $body$
select
personal_motorizado.nombres_motorizado as nombre_motorizado,
personal_motorizado.apellidos_motorizado as apellido_motorizado,
extract (year from entrega.fecha_entrega) as Fecha,
count (personal_motorizado.nombres_motorizado) as numero_entregas
from entrega 
inner join personal_motorizado on personal_motorizado.cod_motorizado=entrega.cod_motorizado
where entrega.cod_cliente=$1
group by extract (year from entrega.fecha_entrega), personal_motorizado.nombres_motorizado,
personal_motorizado.apellidos_motorizado
order by extract (year from entrega.fecha_entrega)
$body$
language sql;


---Ejemplo
select * from totalcliente ('05')
as ("Nombres_Motorizado" character varying
   ,"Apellidos_Motorizado" character varying,
   "Fecha_Entrega" double precision,
   "No_Entregas" bigint);