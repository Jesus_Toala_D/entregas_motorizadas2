---Cursor
do $$
declare
soliclie record;
solcli_datos Cursor for 
select cliente.nombres_cliente as nombres,
cliente.apellidos_cliente as apellidos,
extract (year from solicitud.fecha_solicitud) as fecha,
count (cliente.nombres_cliente) as contar
from solicitud
inner join cliente on solicitud.cod_cliente=cliente.cod_cliente
group by extract (year from solicitud.fecha_solicitud), cliente.nombres_cliente
,cliente.apellidos_cliente;
begin
pen solcli_datos;
fetch solcli_datos into soliclie;
While (found) loop		
RAISE NOTICE 'Año: %, Nombres: %, Apellidos: %, No_Solicitudes: % ',
soliclie.fecha,soliclie.nombres, soliclie.apellidos, soliclie.contar;
fetch solcli_datos into soliclie;
end loop;
end $$
language 'plpgsql';