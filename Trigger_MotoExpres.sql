---Trigger
create or replace function licenciav() returns Trigger
as 
$licenciav$
declare
entregaa date;
expedicionn date;
begin
select 
fecha_entrega into entregaa
from entrega
where entrega.fecha_entrega= new.fecha_entrega;
select 
expedicion_licencia into expedicionn
from personal_motorizado;
if (entregaa > expedicionn) then
raise exception 'Licencia Vencida';
end if;
return new;
End;
$licenciav$
language plpgsql;
create trigger licenciav after
insert on entrega for each row
execute procedure licenciav();

---Ejemplo
INSERT INTO entrega VALUES('08','11','02','02','05','2022/01/23','Entregado','1');
